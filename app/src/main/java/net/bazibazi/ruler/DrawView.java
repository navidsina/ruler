package net.bazibazi.ruler;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.View;

/**
 * Created by sina on 5/4/2017.
 */

public class DrawView extends View {
    Paint paint = new Paint();
    int lowLocation;
    int highLocation;

    public DrawView(Context context, int lowLocation, int highLocation) {
        super(context);
        paint.setColor(Color.BLACK);
        this.lowLocation = lowLocation;
        this.highLocation = highLocation;
    }

    @Override
    public void onDraw(Canvas canvas) {
        canvas.drawLine(0, lowLocation, 2000, lowLocation, paint);
        canvas.drawLine(0, highLocation, 2000, highLocation, paint);
    }

}