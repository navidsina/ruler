package net.bazibazi.ruler;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarFinalValueListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar;

import io.apptik.widget.MultiSlider;

public class MainActivity extends AppCompatActivity {
    double ConversionUnit = 1;
    int leftValue = 0;
    int rightValue = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Remove title bar
        //this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        //Remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);


        setContentView(R.layout.activity_main);

//        // get seekbar from view
//        final CrystalRangeSeekbar rangeSeekbar = (CrystalRangeSeekbar) findViewById(R.id.rangeSeekbar1);
//
//
//        // get min and max text view
//        final TextView tvMin = (TextView) findViewById(R.id.textMin1);
//        final TextView tvMax = (TextView) findViewById(R.id.textMax1);
//
//        // set listener
//        rangeSeekbar.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
//            @Override
//            public void valueChanged(Number minValue, Number maxValue) {
//                tvMin.setText(String.valueOf(minValue));
//                tvMax.setText(String.valueOf(maxValue));
//            }
//        });
//
//        // set final value listener
//        rangeSeekbar.setOnRangeSeekbarFinalValueListener(new OnRangeSeekbarFinalValueListener() {
//            @Override
//            public void finalValue(Number minValue, Number maxValue) {
//                Log.d("CRS=>", String.valueOf(minValue) + " : " + String.valueOf(maxValue));
//            }
//        });


         //get min and max text view
//        final TextView tvMin = (TextView) findViewById(R.id.textMin1);
//        final TextView tvMax = (TextView) findViewById(R.id.textMax1);

        final TextView result = (TextView) findViewById(R.id.result);

        final MultiSlider multiSlider2 = (MultiSlider)findViewById(R.id.range_slider2);
        rightValue = multiSlider2.getMax();
        leftValue = multiSlider2.getMin();

        multiSlider2.setOnThumbValueChangeListener(new MultiSlider.OnThumbValueChangeListener() {
            @Override
            public void onValueChanged(MultiSlider multiSlider, MultiSlider.Thumb thumb, int thumbIndex, int value) {
                if (thumbIndex == 0) {
                    leftValue = value;
                }
                else {
                    rightValue = value;
                }

                result.setText(String.format("%.2f",(ConversionUnit * (rightValue - leftValue))));


//                setContentView(R.layout.activity_main);
//
//                DrawView drawView = new DrawView(MainActivity.this, leftValue, rightValue);
//                drawView.setBackgroundColor(Color.TRANSPARENT);
//                addContentView(drawView, new ViewGroup.LayoutParams( ViewGroup.LayoutParams.MATCH_PARENT , ViewGroup.LayoutParams.MATCH_PARENT ));



            }
        });

        Button button = (Button) findViewById(R.id.calibrate);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText calibrateValueEditText = (EditText) findViewById(R.id.calibrateValueEditText);

                ConversionUnit = Float.parseFloat(calibrateValueEditText.getText().toString()) / (rightValue - leftValue);
                result.setText(calibrateValueEditText.getText().toString());
            }
        });
    }




}
